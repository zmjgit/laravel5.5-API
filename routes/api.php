<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
/*
Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
*/

$api = app('api.router');

//H5活动插件子模块路由。设置了api_token权限认证
$api->version('v1',['middleware'=>'auth:api'],function ($api) {
    $api->any('/mcenter/{d}/{m}/{c}','App\Http\Controllers\Api\V1\ModuleCenterController@center');
});
