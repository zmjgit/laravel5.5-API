<?php

/**
 * 插件服务实现
 */

namespace App\Plug;

use Symfony\Component\VarDumper\Caster\ReflectionCaster;

class PlugService
{

    /**
     * 插件中心服务。转发实现不同的插件类
     * @param $class
     * @param $method
     * @param $param
     * @return mixed
     */
    public function moduleLink($dir = '',$class = '', $method = '', array $param = [])
    {
        $class = 'App\\Plug\\' . $dir . "\\$class";
        $dispatch = new $class($method);
        //call_user_func_array函数可以根据关联数组的键值来传递传输。比invokeArgs反射更灵活
        return call_user_func_array(array($dispatch, $method), $param);
    }

}