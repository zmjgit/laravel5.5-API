<?php
namespace App\Plug;
use Illuminate\Support\ServiceProvider;
use App\Plug\PlugService;
class PlugServiceProvider extends ServiceProvider {

    public function register()
    {
        $this->app->bind('PlugService', function()
        {
            return new PlugService();
        });
    }
}