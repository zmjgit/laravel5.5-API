<?php
namespace App\Http\Controllers\Api\V1;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Plug;

class ModuleCenterController extends Controller
{

    /**
     * 子模块解析（区分大小写）
     * 这里可以做权限控制。精确到具体方法
     * @param $dir   模块目录名
     * @param $class    模块类名
     * @param $method   需要实现的方法名
     * @param Request $request   获取参数
     */
    public function center($dir,$class,$method,Request $request)
    {
        //获取请求的参数，并组成一个关联一维数组
        $parame = $request->all();
        //根据URL调用指定模块/类/方法
        Plug::moduleLink($dir,$class,$method,$parame);

    }
}