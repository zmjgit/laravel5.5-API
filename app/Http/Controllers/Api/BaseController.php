<?php

namespace App\Http\Controllers\Api;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class BaseController extends Controller
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;


    /**
     * 正常请求
     * @param $data
     * @return mixed
     */
    public function json($data){
        return Response::json(array('code'=>200,'data'=>$data));
    }

    /**
     * 请求活动为空的
     */
    protected function noData(){
        return $this->errorJson(400,'No Data');
    }

    /**
     * 请求参数错误
     */
    protected  function noParams(){
        return $this->errorJson(1,'Params Error');
    }

    /**
     * 系统错误
     * @return mixed
     */
    protected function ErrSystem(){
        return $this->errorJson(403,'System Error');
    }

}
